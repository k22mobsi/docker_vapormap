#!/bin/sh
export VAPORMAP_BACKEND=${VAPORMAP_BACKEND}
export VAPORMAP_BACKEND_PORT=${VAPORMAP_BACKEND_PORT}
echo '{ "apiUrl": "http:/{VAPORMAP_BACKEND}:${VAPORMAP_BACKEND_PORT}" }' > ./config.json
python -m http.server